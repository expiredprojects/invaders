package 
{
	import flash.desktop.NativeApplication;
	import flash.events.Event;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.geom.Rectangle;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	import rainhead.GameManager;
	import starling.core.Starling;
	
	[SWF(frameRate="60", width="1024", height="768")]
	public class Main extends Sprite 
	{
		public function Main():void 
		{
			super();
			if (stage) 
			{
				initStarling();
			}
			else this.addEventListener(Event.ADDED_TO_STAGE, onAddedHandler);
		}
		
		private function onAddedHandler(e:Event):void 
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedHandler);
			initStarling();
		}
		
		private function initStarling():void 
		{
			Starling.multitouchEnabled = true;
			
			var starling:Starling = new Starling(GameManager, stage, new Rectangle(0, 0, stage.stageWidth, stage.stageHeight));
			starling.showStats = true;
			starling.start();
		}
	}
}
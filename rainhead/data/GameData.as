package rainhead.data 
{
	public class GameData 
	{
		public static const PLAYER_SHIP_SPEED:int = 3;
		public static const PLAYER_BULLET_SPEED:int = 6;
		public static const ENEMY_BULLET_SPEED:int = 6;
		public static const BULLET_MIN_DELAY:int = 300;
		
		public static const MAX_LIVES:int = 3;
		public static const MAX_ZERO:int = 5;
		public static var LIVES:int = MAX_LIVES;
		public static var SCORE:int = 0;
		
		public static const POINTS_FOR_HIT:int = 125;
		public static const SOUND_BACKGROUND:String = "background";
		public static const SOUND_ENEMY_DIE:String = "enemy_die";
		public static const SOUND_SHOOT_ENEMY:String = "shoot_enemy";
		public static const SOUND_SHOOT_PLAYER:String = "shoot_player";
	}
}
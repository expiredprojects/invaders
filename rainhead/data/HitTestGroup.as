package rainhead.data 
{
	public class HitTestGroup 
	{
		public static const ENEMY:String = "enemy";	
		public static const PLAYER:String = "player";
		public static const BULLET_PLAYER:String = "bullet_player";
		public static const BULLET_ENEMY:String = "bullet_enemy";
	}
}
package rainhead.data 
{
	public class GameObjectsDirection 
	{
		public static const MOVE_NONE:int = 0;
		public static const MOVE_LEFT:int = -1;
		public static const MOVE_RIGHT:int = 1;		
	}
}
package rainhead.sound 
{
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.utils.Dictionary;

	public class SoundManager 
	{
		private var sounds:Dictionary;
		private var channels:Dictionary;
		
		public function SoundManager() 
		{
			sounds = new Dictionary();
			channels = new Dictionary();
		}	
		
		public function addElement(element:Sound, name:String):void 
		{
			if (sounds[name] == null) 
			{
				sounds[name] = element;
			}
		}
		
		public function playSound(name:String, loops:int = 0, volume:Number = 1):void //!!!!!!!!
		{
			channels[name] = Sound(sounds[name]).play(0, loops, new SoundTransform(volume));
		}
		
		public function stopAll():void 
		{
			for each (var element:SoundChannel in channels) 
			{
				element.stop();
			}
		}
	}
}
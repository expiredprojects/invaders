package rainhead.elements 
{
	import flash.geom.Rectangle;
	import rainhead.core.GameObject;
	import rainhead.core.IHitTestObject;
	import rainhead.core.IUpdateObject;
	import rainhead.data.GameData;
	import rainhead.data.HitTestGroup;
	import rainhead.GameManager;
	import starling.textures.TextureAtlas;
	
	public class Bullet extends GameObject implements IHitTestObject, IUpdateObject 
	{
		private var speed:int;
		private var hitGroup:String;
		private var bEnemyBullet:Boolean;
		private var currentGroup:String;
		
		public function Bullet(atlas:TextureAtlas, bEnemyBullet:Boolean) 
		{
			super(atlas, bEnemyBullet?"enemy_ship_shoot":"space_ship_shoot");
			
			if (bEnemyBullet) 
			{
				speed = GameData.ENEMY_BULLET_SPEED;
				hitGroup = HitTestGroup.PLAYER;
				currentGroup = HitTestGroup.BULLET_ENEMY;
			}			
			else 
			{
				speed = -GameData.PLAYER_BULLET_SPEED;
				hitGroup = HitTestGroup.ENEMY;
				currentGroup = HitTestGroup.BULLET_PLAYER;
			}
			
			this.bEnemyBullet = bEnemyBullet;
		}
		
		public function update():void 
		{
			if ((!bEnemyBullet && this.y < -this.height) || (bEnemyBullet && this.y > stage.stageHeight))
			{
				remove();
			}
			else 
			{
				this.y += speed;
				if (GameManager.instance.hitTestManager.checkCollision(this, hitGroup))
				{
					remove();
				}
			}
		}
		
		public function hit(object:IHitTestObject):void
		{
			
		}
		
		public function get hitTestRectange():Rectangle 
		{
			return new Rectangle(this.x , this.y, this.width, this.height);
		}
		
		public function get hitTestGroup():String 
		{
			return HitTestGroup.BULLET_PLAYER;
		}
		
		protected override function remove():void 
		{
			GameManager.instance.hitTestManager.removeElement(this);
			GameManager.instance.removeElement(this);
			super.remove();
		}
	}
}
package rainhead 
{
	import flash.media.AudioPlaybackMode;
	import flash.media.Sound;
	import flash.media.SoundMixer;
	import rainhead.assets.GameAsset;
	import rainhead.core.GameObject;
	import rainhead.core.HitTestManager;
	import rainhead.core.IUpdateObject;
	import rainhead.data.GameData;
	import rainhead.level.LevelManager;
	import rainhead.player.PlayerShip;
	import rainhead.sound.SoundManager;
	import rainhead.ui.GameInterface;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	public class GameManager extends Sprite 
	{		
		private var atlas:TextureAtlas;
		private var gameElements:Vector.<IUpdateObject>;
		private static var _instance:GameManager;
		private var _playerShip:PlayerShip;
		private var _hitTestManager:HitTestManager;
		private var _gameInterface:GameInterface;
		private var levelManager:LevelManager;
		private var _soundManager:SoundManager;
		
		public function GameManager() 
		{
			super();
			_instance = this;
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedHander);
		}
		
		public static function get instance():GameManager 
		{
			return _instance;
		}
		
		public function get playerShip():PlayerShip 
		{
			return _playerShip;
		}
		
		public function get hitTestManager():HitTestManager 
		{
			return _hitTestManager;
		}
		
		public function get gameInterface():GameInterface 
		{
			return _gameInterface;
		}
		
		public function get soundManager():SoundManager 
		{
			return _soundManager;
		}
		
		private function onAddedHander(e:Event):void 
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedHander);
			gameElements = new Vector.<IUpdateObject>();
			
			createTextures();
			createElements();
			startGame();
		}
		
		private function createTextures():void 
		{
			var texture:Texture = Texture.fromBitmap(new GameAsset.AtlasTexture());
			var xml:XML = XML(new GameAsset.AtlasXML());
			atlas = new TextureAtlas(texture, xml);
			
			var fontTexture:Texture = Texture.fromBitmap(new GameAsset.FontTexture());
			var fontXML:XML = XML(new GameAsset.FontXml());
			TextField.registerBitmapFont(new BitmapFont(fontTexture, fontXML));
		}
		
		private function createElements():void 
		{
			SoundMixer.audioPlaybackMode = AudioPlaybackMode.AMBIENT;
			
			_soundManager = new SoundManager();
			_soundManager.addElement(Sound(new GameAsset.SOUND_BACKGROUND()), GameData.SOUND_BACKGROUND);
			_soundManager.addElement(Sound(new GameAsset.SOUND_ENEMY_DIE()), GameData.SOUND_ENEMY_DIE);
			_soundManager.addElement(Sound(new GameAsset.SOUND_SHOOT_ENEMY()), GameData.SOUND_SHOOT_ENEMY);
			_soundManager.addElement(Sound(new GameAsset.SOUND_SHOOT_PLAYER()), GameData.SOUND_SHOOT_PLAYER);
			
			_soundManager.playSound(GameData.SOUND_BACKGROUND, int.MAX_VALUE, 0.5);
			_hitTestManager = new HitTestManager();
			
			var background:GameObject = new GameObject(atlas, "background");
			addChild(background);
			
			//addChild(_hitTestManager.debugSprite);
			
			_gameInterface = new GameInterface(atlas);
			addChild(gameInterface);
			
			levelManager = new LevelManager(atlas);
			addElement(levelManager);
			
			_playerShip = new PlayerShip(atlas);
			addElement(_playerShip);
			_playerShip.y = stage.stageHeight - 180;
			_playerShip.x = int(stage.stageWidth / 2);
			
			_hitTestManager.addElement(_playerShip);
		}
		
		public function startGame():void 
		{
			this.addEventListener(Event.ENTER_FRAME, onEnterFrameHandler);
		}
		
		public function stopGame():void 
		{
			this.removeEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrameHandler);
			levelManager.stopLevel();
		}
		
		public function addElement(element:IUpdateObject):void 
		{
			gameElements.push(element);
			addChild(Sprite(element));
		}
		
		public function removeElement(element:IUpdateObject):void 
		{
			gameElements.splice(gameElements.indexOf(element), 1);
			removeChild(Sprite(element));
		}
		
		private function onEnterFrameHandler(e:Event):void 
		{
			for each (var element:IUpdateObject in gameElements) 
			{
				element.update();
			}
			
			//_hitTestManager.debugDraw();
		}
	}
}
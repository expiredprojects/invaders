package rainhead.ui 
{
	import rainhead.data.GameData;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	public class PlayerHUD extends Sprite 
	{
		private var livesField:TextField;
		private var pointsField:TextField;
		private var scoreField:TextField;
		
		public function PlayerHUD() 
		{
			super();
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedHander);			
		}
		
		private function onAddedHander(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedHander);
			createFields();
		}
		
		private function createFields():void 
		{
			livesField = new TextField(300, 300, "",  "PlasticNo.28", 35, 0xFF9933, false);
			livesField.x = 10;
			livesField.y = 10;
			livesField.vAlign = VAlign.TOP;
			livesField.hAlign = HAlign.LEFT;
			
			addChild(livesField);
			updateLives(GameData.MAX_LIVES);
			
			pointsField = new TextField(300, 300, "",  "PlasticNo.28", 35, 0xFF9933, false);
			pointsField.x = stage.stageWidth - pointsField.width  - 10;
			pointsField.y = 10;
			pointsField.vAlign = VAlign.TOP;
			pointsField.hAlign = HAlign.RIGHT;
			
			addChild(pointsField);
			updatePoints(0);
		}
		
		public function updateLives(lives:int):void 
		{
			livesField.text = "Lives: " + lives;
		}
		
		
		public function updatePoints(score:int):void 
		{
			var fieldText:String = "";
			var scoreLength:int = score.toString().length;
			var loopEnd:int = GameData.MAX_ZERO - scoreLength;
			
			for (var i:int = 0; i < loopEnd; i++) 
			{
				fieldText += "0";
			}
			
			pointsField.text = "Points: " + fieldText + score;
		}
	}
}
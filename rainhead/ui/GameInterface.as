package rainhead.ui 
{
	import rainhead.core.GameObject;
	import rainhead.data.GameObjectsDirection;
	import rainhead.GameManager;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.TextureAtlas;
	
	public class GameInterface extends Sprite 
	{
		private var atlas:TextureAtlas;
		private var leftButton:GameObject;
		private var rightButton:GameObject;
		private var buttonA:GameObject;
		private var _playerHUD:PlayerHUD;
		
		public function GameInterface(atlas:TextureAtlas) 
		{
			super();
			this.atlas = atlas;
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedHander);
		}
		
		private function onAddedHander(e:Event):void 
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedHander);
			createControls();
		}
		
		private function createControls():void 
		{
			leftButton = new GameObject(atlas, "button_left");
			addChild(leftButton);
			leftButton.x = 20;
			leftButton.y = stage.stageHeight - leftButton.height - 20;
			
			rightButton = new GameObject(atlas, "button_right");
			addChild(rightButton);
			rightButton.x = leftButton.x + leftButton.width + 20;
			rightButton.y = leftButton.y;
			
			leftButton.addEventListener(TouchEvent.TOUCH, onTouchHandler);
			rightButton.addEventListener(TouchEvent.TOUCH, onTouchHandler);
			
			buttonA = new GameObject(atlas, "button_shoot");
			addChild(buttonA);
			buttonA.x = stage.stageWidth - buttonA.width - 20;
			buttonA.y = leftButton.y;
			
			buttonA.addEventListener(TouchEvent.TOUCH, onTouchHandler);
			
			_playerHUD = new PlayerHUD();
			addChild(playerHUD);
		}
		
		private function onTouchHandler(e:TouchEvent):void 
		{
			if (e.getTouches(buttonA, TouchPhase.BEGAN).length != 0) 
			{
				GameManager.instance.playerShip.bShooting = true;
			}
			if (e.getTouches(buttonA, TouchPhase.ENDED).length != 0) 
			{
				GameManager.instance.playerShip.bShooting = false;
			}
			
			if (e.getTouches(leftButton, TouchPhase.ENDED).length != 0 || e.getTouches(rightButton, TouchPhase.ENDED).length != 0) 
			{
				GameManager.instance.playerShip.move(GameObjectsDirection.MOVE_NONE);
			}
			
			if (e.getTouches(leftButton, TouchPhase.BEGAN).length != 0)
			{
				GameManager.instance.playerShip.move(GameObjectsDirection.MOVE_LEFT);
			}
			else if (e.getTouches(rightButton, TouchPhase.BEGAN).length != 0)
			{
				GameManager.instance.playerShip.move(GameObjectsDirection.MOVE_RIGHT);
			}
		}
		
		public function get playerHUD():PlayerHUD 
		{
			return _playerHUD;
		}
	}
}
package rainhead.level 
{
	import rainhead.core.IUpdateObject;
	import rainhead.data.GameObjectsDirection;
	import rainhead.enemy.EnemyTypes;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.TextureAtlas;
	
	public class LevelManager extends Sprite implements IUpdateObject
	{
		private var atlas:TextureAtlas;
		private var lines:Vector.<LevelLine>;
		
		public function LevelManager(atlas:TextureAtlas) 
		{
			super();
			this.atlas = atlas;
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedHandler);
		}	
		
		private function onAddedHandler(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedHandler);
			createLevel();
		}
		
		private function createLevel():void 
		{
			lines = new Vector.<LevelLine>();
			
			var line0:LevelLine = new LevelLine(atlas, EnemyTypes.RED_BALL, 3, 12, 8, 1, GameObjectsDirection.MOVE_LEFT);
			line0.y = 400;
			addChild(line0);
			lines.push(line0);
			
			var line1:LevelLine = new LevelLine(atlas, EnemyTypes.RED_BALL, 3, 12, 8, 1, GameObjectsDirection.MOVE_RIGHT);
			line1.y = 340;
			addChild(line1);
			lines.push(line1);
			
			var line2:LevelLine = new LevelLine(atlas, EnemyTypes.GREEN_ALIEN, 2, 8, 8, 3, GameObjectsDirection.MOVE_LEFT);
			line2.y = 230;
			addChild(line2);
			lines.push(line2);
			line2.initShooting(4000, 3000);
			
			var line3:LevelLine = new LevelLine(atlas, EnemyTypes.PURPLE_ALIEN, 3, 5, 25, 3, GameObjectsDirection.MOVE_RIGHT);
			line3.y = 140;
			addChild(line3);
			lines.push(line3);
			line3.initShooting(3000, 2000);
			
			var line4:LevelLine = new LevelLine(atlas, EnemyTypes.BOSS_ALIEN, 10, 1, 0, 6, GameObjectsDirection.MOVE_LEFT);
			line4.y = 50;
			addChild(line4);
			lines.push(line4);
			line4.initShooting(2000, 2000);
		}
	
		public function update():void 
		{
			for each (var line:LevelLine in lines) 
			{
				line.update();
			}
		}
		
		public function stopLevel():void 
		{
			for each (var line:LevelLine in lines) 
			{
				line.stop();
			}
		}
	}
}
package rainhead.level 
{
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import rainhead.core.IUpdateObject;
	import rainhead.data.GameObjectsDirection;
	import rainhead.enemy.Enemy;
	import rainhead.GameManager;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.TextureAtlas;
	
	public class LevelLine extends Sprite implements IUpdateObject
	{
		private var atlas:TextureAtlas;
		private var enemyType:String;
		private var enemyLives:int;
		private var enemyNumber:int;
		private var enemySpace:int;
		private var enemySpeed:int;
		private var currentDirection:int;
		private var enemies:Vector.<Enemy>;
		private var leftMargin:int = 0;
		private var timer:Timer;
		private var delay:int;
		private var randomDelay:int;
		
		public function LevelLine(atlas:TextureAtlas, enemyType:String, enemyLives:int, enemyNumber:int, enemySpace:int, enemySpeed:int, startDirection:int)
		{
			super();
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedHandler);
			this.atlas = atlas;
			this.enemyType = enemyType;
			this.enemyLives = enemyLives;
			this.enemyNumber = enemyNumber;
			this.enemySpace = enemySpace;
			this.enemySpeed = enemySpeed;
			this.currentDirection = startDirection;
		}	
		
		public function update():void 
		{		
			if (currentDirection == GameObjectsDirection.MOVE_LEFT && this.x  + leftMargin < 0) 
			{
				this.x = -leftMargin;
				currentDirection = GameObjectsDirection.MOVE_RIGHT;
			}
			else if (currentDirection == GameObjectsDirection.MOVE_RIGHT && this.x + this.width + leftMargin > stage.stageWidth) 
			{
				this.x = stage.stageWidth - this.width - leftMargin;
				currentDirection = GameObjectsDirection.MOVE_LEFT;
			}
			
			this.x += currentDirection * this.enemySpeed;
		}
		
		private function createEnemies():void 
		{
			enemies = new Vector.<Enemy>();
			var nextX:int = 0;
			
			for (var i:int = 0; i < enemyNumber; i++) 
			{
				var enemy:Enemy = new Enemy(atlas, enemyType, enemyLives);
				addChild(enemy);
				
				enemy.addEventListener(Event.REMOVED_FROM_STAGE, onRemoveHandler);
				enemies.push(enemy);
				
				GameManager.instance.hitTestManager.addElement(enemy);
				
				enemy.x = nextX;
				nextX += enemy.width + enemySpace;
			}
			
			this.x = int((stage.stageWidth - this.width) / 2);
		}
		
		private function onRemoveHandler(e:Event):void 
		{
			var enemy:Enemy = Enemy(e.currentTarget);
			var index:int = enemies.indexOf(enemy);
			enemies.splice(index, 1);
			
			if (index == 0) 
			{
				var minX:int = int.MAX_VALUE;
				
				for each (var currentEnemy:Enemy in enemies) 
				{
					if (currentEnemy.x < minX) 
					{
						minX = currentEnemy.x;
					}
				}
				
				leftMargin = minX;
			}
			
			e.currentTarget.removeEventListener(Event.REMOVED_FROM_STAGE, onRemoveHandler);	
			if (enemies.length == 0) 
			{
				stop();
			}
		}
		
		public function initShooting(delay:int, randomDelay:int):void 
		{
			this.delay = delay;
			this.randomDelay = randomDelay;
			createTimer();
		}
		
		public function stop():void 
		{
			if (timer) 
			{
				timer.stop();
				timer.removeEventListener(TimerEvent.TIMER, onTimerHandler);
			}
		}
		
		private function onTimerHandler(e:TimerEvent):void 
		{
			timer.stop();
			timer.removeEventListener(TimerEvent.TIMER, onTimerHandler);
			createTimer();
			
			var randomIndex:int = Math.round(Math.random() * (enemies.length - 1));
			enemies[randomIndex].shoot();
		}
		
		private function onAddedHandler(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedHandler);
			createEnemies();
		}
		
		private function createTimer():void 
		{
			timer = new Timer(int(delay + Math.random() * randomDelay));
			timer.addEventListener(TimerEvent.TIMER, onTimerHandler);
			timer.start();
		}
	}
}
package rainhead.core 
{
	public interface IUpdateObject 
	{
		function update():void;
	}
}
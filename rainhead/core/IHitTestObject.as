package rainhead.core 
{
	import flash.geom.Rectangle;
	
	public interface IHitTestObject 
	{
		function get hitTestRectange():Rectangle;
		function get hitTestGroup():String;
		function hit(object:IHitTestObject):void;
	}	
}
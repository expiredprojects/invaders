package rainhead.core 
{
	import flash.geom.Rectangle;
	import starling.display.Quad;
	import starling.display.Sprite;
	public class HitTestManager 
	{
		private var elements:Vector.<IHitTestObject>;
		private var quads:Vector.<Quad>;
		private var _debugSprite:Sprite;
		
		public function HitTestManager() 
		{
			elements = new Vector.<IHitTestObject>();
			quads = new Vector.<Quad>();
			_debugSprite = new Sprite();
		}	
		
		public function checkCollision(element:IHitTestObject, group:String):Boolean
		{
			for each (var hitElement:IHitTestObject in elements) 
			{
				if (hitElement.hitTestGroup == group && hitElement.hitTestRectange.intersects(element.hitTestRectange)) 
				{
					hitElement.hit(element);
					return true;
				}
			}
			
			return false;
		}
		
		public function debugDraw():void 
		{
			for each (var currentQuad:Quad in quads) 
			{
				_debugSprite.removeChild(currentQuad);
			}
			
			quads = new Vector.<Quad>();
			
			for each (var hitElement:IHitTestObject in elements) 
			{
				var rect:Rectangle = hitElement.hitTestRectange;
				var quad:Quad = new Quad(rect.width, rect.height, 0x334433);
				quad.x = rect.x;
				quad.y = rect.y;
				_debugSprite.addChild(quad);
				quads.push(quad);
			}
		}
		
		public function addElement(element:IHitTestObject):void 
		{
			elements.push(element);
		}
		
		public function removeElement(element:IHitTestObject):void 
		{
			elements.splice(elements.indexOf(element), 1);
		}
		
		public function get debugSprite():Sprite 
		{
			return _debugSprite;
		}
	}
}
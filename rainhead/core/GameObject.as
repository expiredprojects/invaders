package rainhead.core 
{
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.TextureAtlas;
	
	public class GameObject extends Sprite 
	{
		protected var atlas:TextureAtlas;
		protected var objectImage:Image;
		
		public function GameObject(atlas:TextureAtlas, textureName:String) 
		{
			super();
			this.atlas = atlas;
			objectImage = new Image(atlas.getTexture(textureName));
			addChild(objectImage);
		}
		
		protected function remove():void 
		{
			removeChild(objectImage);
			objectImage = null;
		}
	}
}
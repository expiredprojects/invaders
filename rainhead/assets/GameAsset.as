package rainhead.assets 
{
	public class GameAsset 
	{
		[Embed(source="../../../assets/assets.png")]
		public static const AtlasTexture:Class;
		
		[Embed(source="../../../assets/assets.xml", mimeType = "application/octet-stream")]
		public static const AtlasXML:Class;
		
		[Embed(source="../../../assets/game_font.fnt", mimeType="application/octet-stream")]
		public static const FontXml:Class;

		[Embed(source="../../../assets/game_font.png")]
		public static const FontTexture:Class;
		
		[Embed(source="../../../assets/sounds/background.mp3")]
		public static const SOUND_BACKGROUND:Class;
		
		[Embed(source="../../../assets/sounds/enemy_die.mp3")]
		public static const SOUND_ENEMY_DIE:Class;		
		
		[Embed(source="../../../assets/sounds/shoot_enemy.mp3")]
		public static const SOUND_SHOOT_ENEMY:Class;
		
		[Embed(source="../../../assets/sounds/shoot_player.mp3")]
		public static const SOUND_SHOOT_PLAYER:Class;
		
		public function GameAsset() 
		{
			
		}
	}
}
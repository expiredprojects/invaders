package rainhead.enemy 
{
	import flash.geom.Rectangle;
	import rainhead.core.GameObject;
	import rainhead.core.IHitTestObject;
	import rainhead.data.GameData;
	import rainhead.data.HitTestGroup;
	import rainhead.elements.Bullet;
	import rainhead.GameManager;
	import starling.textures.TextureAtlas;
	
	public class Enemy extends GameObject implements IHitTestObject 
	{
		private var lives:int;
		
		public function Enemy(atlas:TextureAtlas, textureName:String, lives:int = 1) 
		{
			super(atlas, textureName);
			this.lives = lives;
		}
		
		public function get hitTestRectange():Rectangle 
		{
			return new Rectangle(this.x + this.parent.x, this.y + this.parent.y, this.width, this.height);
		}
		
		public function get hitTestGroup():String 
		{
			return HitTestGroup.ENEMY;
		}
		
		public function hit(object:IHitTestObject):void
		{
			lives--;
			if (lives <= 0) 
			{
				destroyEnemy();
			}
		}
		
		public function shoot():void 
		{
			createBullet();
		}
		
		private function createBullet():void 
		{
			var bullet:Bullet = new Bullet(atlas, true);
			bullet.x = int(this.x  + this.parent.x + this.width /2 - bullet.width / 2);
			bullet.y = int(this.y + this.parent.y + this.height / 2);
			
			GameManager.instance.addElement(bullet);
			GameManager.instance.hitTestManager.addElement(bullet);
			GameManager.instance.soundManager.playSound(GameData.SOUND_SHOOT_ENEMY);
		}
		
		private function destroyEnemy():void 
		{
			GameManager.instance.hitTestManager.removeElement(this);
			this.parent.removeChild(this);
			
			GameData.SCORE += GameData.POINTS_FOR_HIT;
			GameManager.instance.gameInterface.playerHUD.updatePoints(GameData.SCORE);
			GameManager.instance.soundManager.playSound(GameData.SOUND_ENEMY_DIE);
			
			remove();
		}
	}
}
package rainhead.enemy 
{
	public class EnemyTypes 
	{
		public static const RED_BALL:String = "enemy_1";
		public static const GREEN_ALIEN:String = "enemy_2";
		public static const PURPLE_ALIEN:String = "enemy_3";
		public static const BOSS_ALIEN:String = "enemy_4";
	}
}
package rainhead.player 
{
	import flash.geom.Rectangle;
	import rainhead.core.GameObject;
	import rainhead.core.IHitTestObject;
	import rainhead.core.IUpdateObject;
	import rainhead.data.GameData;
	import rainhead.data.GameObjectsDirection;
	import rainhead.data.HitTestGroup;
	import rainhead.elements.Bullet;
	import rainhead.GameManager;
	import starling.textures.TextureAtlas;
	
	public class PlayerShip extends GameObject implements IHitTestObject, IUpdateObject
	{	
		private var direction:int = GameObjectsDirection.MOVE_NONE;
		private var _bShooting:Boolean;
		private var lastBulletTime:Number = 0;
		private var spriteWidth:int;
		private var spriteHeight:int;
		
		public function PlayerShip(atlas:TextureAtlas) 
		{
			super(atlas, "player_ship");
			objectImage.x = int( -objectImage.width / 2);
			objectImage.y = int( -objectImage.height / 2);
			this.spriteWidth = objectImage.width;
			this.spriteHeight = objectImage.height;
		}
		
		public function update():void 
		{
			var currentTime:Number = new Date().time;
			
			if (_bShooting && currentTime - lastBulletTime > GameData.BULLET_MIN_DELAY) 
			{
				lastBulletTime = currentTime;
				createBullet();
			}
			
			switch (this.direction) 
			{
				case GameObjectsDirection.MOVE_LEFT:
				{
					this.x -= GameData.PLAYER_SHIP_SPEED;
					this.rotation = -15 * Math.PI / 180;
					break;
				}
				case GameObjectsDirection.MOVE_RIGHT:
				{
					this.x += GameData.PLAYER_SHIP_SPEED;
					this.rotation = 15 * Math.PI / 180;
					break;
				}
				case GameObjectsDirection.MOVE_NONE:
				{
					this.rotation = 0;
					break;
				}
			}
			
			if (this.x  - int(this.width / 2) < 0) 
			{
				this.x = int(this.width / 2);
			}
			else if (this.x + this.width / 2 > stage.stageWidth) 
			{
				this.x = stage.stageWidth - this.width / 2;
			}
		}
		
		public function move(direction:int):void 
		{
			this.direction = direction;
		}
		
		public function get hitTestRectange():Rectangle 
		{
			if (rotation != 0) 
			{
				return new Rectangle(this.x - this.width/2  + 10, this.y - this.height/2 + 10, this.spriteWidth, this.spriteHeight);
			}
			
			return new Rectangle(this.x - this.width/2 , this.y - this.height/2, this.spriteWidth, this.spriteHeight);
		}
		
		public function get hitTestGroup():String 
		{
			return HitTestGroup.PLAYER;
		}
		
		public function hit(object:IHitTestObject):void
		{
			GameData.LIVES--;
			GameManager.instance.gameInterface.playerHUD.updateLives(GameData.LIVES);
			
			if (GameData.LIVES == 0) 
			{
				GameManager.instance.stopGame();
			}
		}
		
		private function createBullet():void 
		{
			var bullet:Bullet = new Bullet(atlas, false);
			bullet.x = int(this.x - (bullet.width / 2));
			bullet.y = int(this.y - (this.height / 2));
			
			GameManager.instance.addElement(bullet);
			GameManager.instance.hitTestManager.addElement(bullet);
			GameManager.instance.soundManager.playSound(GameData.SOUND_SHOOT_PLAYER);
		}
		
		public function get bShooting():Boolean 
		{
			return _bShooting;
		}
		
		public function set bShooting(value:Boolean):void 
		{
			_bShooting = value;
		}
	}
}